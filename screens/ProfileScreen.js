import React from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  View,
  Image,
  Switch,
  AsyncStorage,
  Button,
  TouchableOpacity
} from "react-native";

import FileSystem from "react-native-filesystem";
// import RNFS from "react-native-fs";

// SOME OF THE RESIZE LOGIC I WOULD HAVE USED
/*
window.resize = (function() {

    'use strict';

    function Resize() {
        //
    }

    Resize.prototype = {

        init: function(outputQuality) {
            this.outputQuality = (outputQuality === 'undefined' ? 0.8 : outputQuality);
        },

        photo: function(file, maxSize, outputType, callback) {
            var _this = this;

            var reader = new FileReader();
            reader.onload = function(readerEvent) {
                _this.resize(readerEvent.target.result, maxSize, outputType, callback);
            }

            return reader.readAsDataURL(file);
        },

        resize: function(dataURL, maxSize, outputType, callback) {
            var _this = this;

            var image = new Image();
            image.onload = function(imageEvent) {

                // Resize image
                var canvas = document.createElement('canvas'),
                    width = image.width,
                    height = image.height;
                if (width > height) {
                    if (width > maxSize) {
                        height *= maxSize / width;
                        width = maxSize;
                    }
                } else {
                    if (height > maxSize) {
                        width *= maxSize / height;
                        height = maxSize;
                    }
                }
                canvas.width = width;
                canvas.height = height;
                canvas.getContext('2d').drawImage(image, 0, 0, width, height);

                _this.output(canvas, outputType, callback);

            }
            image.src = dataURL;

        },

        output: function(canvas, outputType, callback) {

            switch (outputType) {

                case 'file':
                    canvas.toBlob(function(blob) {
                        callback(blob);
                    }, 'image/jpeg', 0.8);
                    break;

                case 'dataURL':
                    callback(canvas.toDataURL('image/jpeg', 0.8));
                    break;

            }

        }

    };

    return Resize;

}());
*/

/*
$scope.encodeImageFileAsURL = () => {
    document.getElementById("imgTest").innerHTML = '';
    var filesSelected = document.getElementById("inputFileToLoad").files;

    if (filesSelected.length > 0) {
        var fileToLoad = filesSelected[0];

        var fileReader = new FileReader();

        fileReader.onload = function (fileLoadedEvent) {
            var srcData = fileLoadedEvent.target.result || 'https://www.google.com/url?sa=i&source=images&cd=&cad=rja&uact=8&ved=2ahUKEwja0qr__8zcAhVCj1QKHQSWCvUQjRx6BAgBEAU&url=https%3A%2F%2Fwww.weedcircles.com%2Fcannabis-consumer-products%2Fcontainers-and-bottles%2Flisting%2Fthc-label-solutions-129&psig=AOvVaw2rt6TpjkoztpVjv0CfEDkF&ust=1533252280109083'

            var newImage = document.createElement('img');

            fetch(srcData).then(function (response) {
                return response.blob()
            }).then(function (blob) {

                var resize = new window.resize();
                resize.init();

                resize.resize(srcData, 800, 'dataURL', function (thumbnail) {

                    newImage.src = thumbnail;
                    $scope.user.file = thumbnail;
                    document.getElementById("imgTest").innerHTML = newImage.outerHTML;
                    $('#imgTest').addClass('card');

                });

            });

        }

        fileReader.readAsDataURL(fileToLoad);

    }
}

*/
export default class ProfileScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fileContent: "",
      profile: {
        name: "John Smith",
        phone: "2816853143",
        email: "j.smith@gmail.com",
        dot: "72373246",
        notifications: false
      }
    };
    this.newDate = new Date().getTime();
  }

  static navigationOptions = {
    title: "Profile"
  };
  _signOutAsync = async ({ navigation }) => {
    await AsyncStorage.clear();
    this.props.navigation.navigate("Auth");
  };

  swapImage() {
    console.log("TEST");
    // RNFS.read("D:\\AllRand\\facts-api-handler\\test.txt");
  }
  render() {
    const { name, phone, email, dot, notifications } = this.state.profile;

    return (
      <ScrollView contentContainerStyle={styles.profile_container}>
        <View style={styles.flex_container}>
          <View style={styles.profile_header}>
            <View style={styles.circle}>
              <TouchableOpacity>
                <Image
                  style={styles.image_cirlce}
                  source={{
                    uri:
                      "https://facebook.github.io/react-native/docs/assets/favicon.png"
                  }}
                />
              </TouchableOpacity>

              {/* <Button title="WHAT" onPress={this.swapImage} />*/}
            </View>
          </View>
          <View style={styles.content_container}>
            <Text style={styles.profile_label}>Name:</Text>
            <TextInput
              style={styles.profile_input}
              onChangeText={text => this.setState({ name })}
              value={name}
            />

            <Text style={styles.profile_label}>Phone:</Text>
            <TextInput
              style={styles.profile_input}
              onChangeText={text => this.setState({ phone })}
              value={phone}
            />

            <Text style={styles.profile_label}>Email:</Text>
            <TextInput
              style={styles.profile_input}
              onChangeText={text => this.setState({ email })}
              value={email}
            />
            <Text style={styles.profile_label}>DOT:</Text>
            <TextInput
              style={styles.profile_input}
              onChangeText={text => this.setState({ dot })}
              value={dot}
            />
            <Text style={styles.profile_label}>Enable Notifactions:</Text>
            <Switch />
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  profile_container: {
    flexGrow: 1,
    justifyContent: "space-between"
    // alignItems: "center"
    // flex: 1,
    // flexDirection: "column",
    // backgroundColor: "#fff",
    // height: 100
    // paddingTop: 15,
    // padding: 25
  },
  flex_container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "#fff"
  },
  profile_header: {
    flex: 1,
    minHeight: 200,
    backgroundColor: "#0079b4",
    alignItems: "center",
    justifyContent: "center"
  },
  profile_label: {
    fontSize: 20,
    marginBottom: 5
  },
  profile_input: {
    height: 50,
    marginBottom: 5
  },
  image_cirlce: {
    width: 100,
    height: 100,
    borderRadius: 100,
    borderColor: "#fff",
    borderWidth: 5
  },
  content_container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: "#fff",
    flexDirection: "column",
    padding: 25,
    minHeight: 100
  }
});
