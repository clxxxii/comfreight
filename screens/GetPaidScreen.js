import React from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  View,
  Image,
  Switch
} from "react-native";

import FAB from "react-native-fab";
import { Icon } from "react-native-elements";

import InvoiceCard from "../components/InvoiceCard";

export default class GetPaidScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = { text: "Useless Placeholder" };
    this.newDate = new Date().toLocaleDateString();
    this.invoices = [
      {
        bill: "TAL",
        amount: 12000,
        load: 12334,
        timestamp: this.newDate,
        status: "PENDING"
      },
      {
        bill: "Target",
        amount: 1000,
        load: 44388,
        timestamp: this.newDate,
        status: "APPROVED"
      },
      {
        bill: "Walmart",
        amount: 13000,
        load: 11124,
        timestamp: this.newDate,
        status: "Document Issue"
      },
      {
        bill: "TAL",
        amount: 12000,
        load: 19334,
        timestamp: this.newDate,
        status: "PENDING"
      },
      {
        bill: "Spoons For Less",
        amount: 12000,
        load: 12434,
        timestamp: this.newDate,
        status: "PENDING"
      }
    ];
  }

  static navigationOptions = {
    title: "Get Paid"
  };

  render() {
    return (
      <ScrollView style={styles.content_container}>
        <View>
          <Text style={styles.paid_header}>Your recent payments requests</Text>
        </View>

        <View style={styles.paid_stats}>
          <View style={styles.paid_stats_left}>
            <Text style={styles.paid_stats_content}>
              <Text style={styles.paid_title}>Total Revenue </Text>
              <Text style={styles.paid_data_green}>$4,500.00</Text>
            </Text>
            <Text style={styles.paid_stats_content}>
              <Text style={styles.paid_title}>Reserve Balance </Text>
              <Text style={styles.paid_data_grey}>$0</Text>
            </Text>
          </View>
          <View style={styles.paid_stats_right}>
            <Text style={styles.paid_stats_content}>
              <Text style={styles.paid_title}>Total Outstanding </Text>
              <Text style={styles.paid_data_red}>$1,100.00</Text>
            </Text>
            <Text style={styles.paid_stats_content}>
              <Text style={styles.paid_title}>Your Rate </Text>
              <Text style={styles.paid_data_green}>3.5%</Text>
            </Text>
          </View>
        </View>

        <View>{this.renderInvoices()}</View>
      </ScrollView>
    );
  }

  renderInvoices() {
    return this.invoices.map(invoice => {
      return (
        <View key={invoice.load} style={styles.invoice_container}>
          <InvoiceCard
            amount={invoice.amount}
            bill={invoice.bill}
            load={invoice.load}
            timestamp={invoice.timestamp}
            status={invoice.status}
          />
        </View>
      );
    });
  }
}

const styles = StyleSheet.create({
  fab: {
    position: "absolute",
    margin: 16,
    right: 0,
    bottom: 0
  },
  content_container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: "#fff",
    flexDirection: "column",
    padding: 25
  },
  paid_header: {
    color: "#657180",
    fontSize: 25,
    paddingBottom: 15
  },
  paid_stats: {
    flexDirection: "row",
    height: 100
  },
  paid_stats_content: {
    paddingBottom: 10
  },
  paid_stats_left: {
    flex: 1
  },
  paid_stats_right: {
    flex: 1,
    alignItems: "flex-end"
  },
  paid_title: {
    fontWeight: "bold"
  },
  paid_data_red: {
    color: "red"
  },
  paid_data_green: {
    color: "green"
  },
  paid_data_grey: {
    color: "grey"
  },
  invoice_container: {
    paddingBottom: 10,
    paddingTop: 10
  }
});
