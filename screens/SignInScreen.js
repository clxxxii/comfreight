// 475363122293-ra9ocia999j8p3imvlosln1b92bnb8pm.apps.googleusercontent.com
// 1046509741786-ste6p6v7knjduimt0u10eqp8bmp3f0pm.apps.googleusercontent.com
import React from "react";
import { StyleSheet, Text, View, Button } from "react-native";
import Expo from "expo";
import LoggedInPage from "../components/LoggedInPage";
import LoginPage from "../components/LoginPage";

// reference
// https://reactnavigation.org/docs/en/auth-flow.html
// https://blog.expo.io/google-sign-in-with-react-native-and-expo-9cac6c392f0e

export default class SignInScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = { signedIn: false, name: "", photoUrl: "" };
  }

  static navigationOptions = {
    title: "Please sign in"
  };

  signIn = async () => {
    try {
      const result = await Expo.Google.logInAsync({
        androidClientId:
          "1046509741786-ste6p6v7knjduimt0u10eqp8bmp3f0pm.apps.googleusercontent.com",
        //iosClientId: YOUR_CLIENT_ID_HERE,  <-- if you use iOS
        scopes: ["profile", "email"]
      });

      if (result.type === "success") {
        console.log(result);
        this.setState({
          signedIn: true,
          name: result.user.name,
          photoUrl: result.user.photoUrl,
          token: result.accessToken
        });
      } else {
        console.log("Cancelled");
      }
    } catch (e) {
      console.log("error", e);
    }
  };
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        {this.state.signedIn ? (
          <LoggedInPage
            navigate={navigate}
            token={this.state.token}
            name={this.state.name}
            photoUrl={this.state.photoUrl}
          />
        ) : (
          <LoginPage signIn={this.signIn} />
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  },
  header: {
    fontSize: 25
  }
});
