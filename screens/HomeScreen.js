import React from "react";
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  WebView,
  Dimensions,
  AsyncStorage
} from "react-native";

import AlertCard from "../components/AlertCard";
const { width } = Dimensions.get("window");

export default class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
    this.alerts = [
      {
        message: "Your request is APPROVED",
        howLongAgo: "1 day ago",
        key: Number(Math.floor(Math.random() * 1000))
      },
      {
        message: "Document issue on your request.",
        howLongAgo: "4 days ago",
        key: Number(Math.floor(Math.random() * 1000))
      },
      {
        message: "Please come into the Office",
        howLongAgo: "0 days ago",
        key: Number(Math.floor(Math.random() * 1000))
      },
      {
        message: "Welcome to the Team",
        howLongAgo: "0 days ago",
        key: Number(Math.floor(Math.random() * 1000))
      },
      {
        message: "Your Mac is in!",
        howLongAgo: "1 day ago",
        key: Number(Math.floor(Math.random() * 1000))
      }
    ];
  }
  static navigationOptions = {
    header: null
  };

  componentDidMount() {
    setTimeout(() => {
      this.scrollView.scrollTo({ x: -30 });
    }, 1); // scroll view position fix
  }
  render() {
    return (
      <View style={styles.content_container}>
        <ScrollView
          style={styles.container}
          contentContainerStyle={styles.contentContainer}
        >
          <View>
            <Text style={styles.paid_header}>Welcome to HAULPAY</Text>
          </View>

          <View style={{ height: 300 }}>
            <WebView
              style={styles.WebViewContainer}
              javaScriptEnabled={true}
              domStorageEnabled={true}
              source={{ uri: "https://www.youtube.com/watch?v=O727LCYFG-Y" }}
            />
          </View>

          <View>
            <ScrollView
              ref={scrollView => {
                this.scrollView = scrollView;
              }}
              style={styles.__container}
              pagingEnabled={true}
              horizontal={true}
              decelerationRate={0}
              snapToInterval={width - 60}
              snapToAlignment={"center"}
              contentInset={{
                top: 0,
                left: 0,
                bottom: 0,
                right: 30
              }}
            >
              {this.renderAlerts()}
            </ScrollView>
          </View>

          <View style={styles.helpContainer}>
            <TouchableOpacity
              onPress={this._signOutAsync}
              style={styles.helpLink}
            >
              <Text style={styles.helpLinkText}>Sign out</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  }

  renderAlerts() {
    return this.alerts.map(alert => {
      return (
        <View key={alert.key} style={styles.invoice_container}>
          <AlertCard message={alert.message} howLongAgo={alert.howLongAgo} />
        </View>
      );
    });
  }

  _signOutAsync = async ({ navigation }) => {
    await AsyncStorage.clear();
    this.props.navigation.navigate("Auth");
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  },
  content_container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: "#fff",
    flexDirection: "column",
    padding: 25
  },
  contentContainer: {
    paddingTop: 30
  },
  helpContainer: {
    marginTop: 15,
    alignItems: "center"
  },
  helpLink: {
    paddingVertical: 15
  },
  helpLinkText: {
    fontSize: 14,
    color: "#2e78b7"
  },
  WebViewContainer: {
    marginTop: Platform.OS == "ios" ? 20 : 0
  },
  slider_card: {
    // marginTop: 100,
    backgroundColor: "blue",
    width: width - width / 2,
    margin: 10,
    height: 150,
    borderRadius: 3,
    paddingHorizontal: 30
  },
  paid_header: {
    color: "#657180",
    fontSize: 25,
    paddingBottom: 15
  }
});
