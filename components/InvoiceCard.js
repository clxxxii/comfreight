import React, { Component } from "react";
import { Text, View, StyleSheet } from "react-native";

export default class InvoiceCard extends Component {
  render() {
    return (
      <View style={styles.invoice}>
        <View style={styles.invoice_content}>
          <Text>Bill: {this.props.bill}</Text>
          <Text>Load: {this.props.load}</Text>
          <Text>Amount: {this.props.amount}</Text>
          <Text>timestamp: {this.props.timestamp}</Text>
          <Text>Status: {this.props.status}</Text>
        </View>
        <View style={styles.invoice_footer}>
          <Text>Requested At: {this.props.timestamp}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  invoice: {
    borderWidth: 1,
    borderRadius: 3,
    borderColor: "#ddd",
    borderBottomWidth: 0,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 3
  },
  invoice_content: {
    padding: 10,
    backgroundColor: "#f0f2f4"
  },
  invoice_footer: {
    padding: 10,
    backgroundColor: "#fff"
  }
});
