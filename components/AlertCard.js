import React, { Component } from "react";
import { Text, View, StyleSheet, Dimensions } from "react-native";
const { width } = Dimensions.get("window");

export default class AlertCard extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={styles.invoice}>
        <View style={styles.invoice_content}>
          <Text>{this.props.message}</Text>
          <Text>{this.props.howLongAgo}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  invoice: {
    borderWidth: 1,
    borderRadius: 3,
    borderColor: "#ddd",
    borderBottomWidth: 0,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 3,
    marginRight: 5,
    marginLeft: 5,
    height: 150
  },
  invoice_content: {
    padding: 10,
    backgroundColor: "#f0f2f4",
    height: 150
  },
  slider_card: {
    backgroundColor: "blue",
    width: width - width / 2,
    margin: 10,
    height: 150,
    borderRadius: 3,
    paddingHorizontal: 30
  }
});
