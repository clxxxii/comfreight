import React from "react";
import { StyleSheet, Text, View, Button } from "react-native";

export default class LoginPage extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <View>
        <Text style={styles.headerTitle}>Sign In With Google</Text>
        <Button
          title="Sign in with Google"
          onPress={() => this.props.signIn()}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  },
  headerTitle: {
    fontSize: 25,
    marginBottom: 50
  },
  header: {
    fontSize: 25
  }
});
