import React from "react";
import { StyleSheet, Text, View, Image, AsyncStorage } from "react-native";

export default class LoggedInPage extends React.Component {
  constructor(props) {
    super(props);
  }

  static navigationOptions = {
    title: "Thanks!"
  };

  componentDidMount() {
    setTimeout(() => {
      this._signInAsync();
    }, 3000);
  }

  _signInAsync = async () => {
    await AsyncStorage.setItem("userToken", this.props.token);
    this.props.navigate("App");
  };

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.header}>Welcome:</Text>
        <Text style={styles.header}>{this.props.name}</Text>
        <Image style={styles.image} source={{ uri: this.props.photoUrl }} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  },
  header: {
    fontSize: 25
  },
  image: {
    marginTop: 15,
    width: 150,
    height: 150,
    borderColor: "rgba(0,0,0,0.2)",
    borderWidth: 3,
    borderRadius: 150
  }
});
