import React from "react";
import { createSwitchNavigator, createStackNavigator } from "react-navigation";

import AuthLoadingScreen from "../screens/AuthLoadingScreen";
import MainTabNavigator from "./MainTabNavigator";
import SignInScreen from "../screens/SignInScreen";

export default createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    App: MainTabNavigator,
    Auth: SignInScreen
  },
  {
    initialRouteName: "AuthLoading"
  }
);
